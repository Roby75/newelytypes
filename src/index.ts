export interface Card {
	suite: string;
	rank: string;
}

export interface Phase {
	cards: Card[];
	sitbets: SitBet[];
}
export interface SitBet {
	bet: number;
	balance: number;
}
export interface SitSummary {
	isFold: boolean;
	cards: Card[];
	winloss: number;
}

export interface HandLog {
	key: string;
	time: string;
	preflop: Phase;
	flop: Phase;
	turn: Phase;
	river: Phase;
	headers: { username: string }[];
	summary: SitSummary[];
	fullHistory: string[];
}

const testHL: HandLog = {
	key: 'tab1-small-1000998',
	time: new Date().toISOString(),
	headers: [
		{ username: 'User' },
		{ username: 'User' },
		{ username: 'User' },
		{ username: 'User' },
		{ username: 'User' },
	],
	preflop: {
		cards: [],
		sitbets: [
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
		],
	},
	flop: {
		cards: [
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
		],
		sitbets: [
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
		],
	},
	turn: {
		cards: [
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
		],
		sitbets: [
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
		],
	},
	river: {
		cards: [
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
			{ rank: 'A', suite: 'S' },
		],
		sitbets: [
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
			{ balance: 100, bet: 100 },
		],
	},
	summary: [
		{
			cards: [
				{ rank: 'A', suite: 'S' },
				{ rank: 'A', suite: 'S' },
			],
			isFold: false,
			winloss: 100,
		},
		{
			cards: [
				{ rank: 'A', suite: 'S' },
				{ rank: 'A', suite: 'S' },
			],
			isFold: false,
			winloss: 100,
		},
		{
			cards: [
				{ rank: 'A', suite: 'S' },
				{ rank: 'A', suite: 'S' },
			],
			isFold: false,
			winloss: 100,
		},
		{
			cards: [
				{ rank: 'A', suite: 'S' },
				{ rank: 'A', suite: 'S' },
			],
			isFold: false,
			winloss: 100,
		},
		{
			cards: [
				{ rank: 'A', suite: 'S' },
				{ rank: 'A', suite: 'S' },
			],
			isFold: false,
			winloss: 100,
		},
	],
	fullHistory: [],
};

export const makeHl = (): HandLog => {
	return {
		headers: [{ username: '' }, { username: '' }, { username: '' }, { username: '' }, { username: '' }],
		key: '',
		time: '',
		flop: {
			cards: [],
			sitbets: [
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
			],
		},
		preflop: {
			cards: [],
			sitbets: [
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
			],
		},
		turn: {
			cards: [],
			sitbets: [
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
			],
		},
		river: {
			cards: [],
			sitbets: [
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
				{ balance: 0, bet: 0 },
			],
		},
		summary: [
			{ cards: [], isFold: true, winloss: 0 },
			{ cards: [], isFold: true, winloss: 0 },
			{ cards: [], isFold: true, winloss: 0 },
			{ cards: [], isFold: true, winloss: 0 },
			{ cards: [], isFold: true, winloss: 0 },
		],
		fullHistory: [],
	};
};
