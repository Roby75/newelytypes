"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeHl = void 0;
var testHL = {
    key: 'tab1-small-1000998',
    time: new Date().toISOString(),
    headers: [
        { username: 'User' },
        { username: 'User' },
        { username: 'User' },
        { username: 'User' },
        { username: 'User' },
    ],
    preflop: {
        cards: [],
        sitbets: [
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
        ],
    },
    flop: {
        cards: [
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
        ],
        sitbets: [
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
        ],
    },
    turn: {
        cards: [
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
        ],
        sitbets: [
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
        ],
    },
    river: {
        cards: [
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
            { rank: 'A', suite: 'S' },
        ],
        sitbets: [
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
            { balance: 100, bet: 100 },
        ],
    },
    summary: [
        {
            cards: [
                { rank: 'A', suite: 'S' },
                { rank: 'A', suite: 'S' },
            ],
            isFold: false,
            winloss: 100,
        },
        {
            cards: [
                { rank: 'A', suite: 'S' },
                { rank: 'A', suite: 'S' },
            ],
            isFold: false,
            winloss: 100,
        },
        {
            cards: [
                { rank: 'A', suite: 'S' },
                { rank: 'A', suite: 'S' },
            ],
            isFold: false,
            winloss: 100,
        },
        {
            cards: [
                { rank: 'A', suite: 'S' },
                { rank: 'A', suite: 'S' },
            ],
            isFold: false,
            winloss: 100,
        },
        {
            cards: [
                { rank: 'A', suite: 'S' },
                { rank: 'A', suite: 'S' },
            ],
            isFold: false,
            winloss: 100,
        },
    ],
    fullHistory: [],
};
var makeHl = function () {
    return {
        headers: [{ username: '' }, { username: '' }, { username: '' }, { username: '' }, { username: '' }],
        key: '',
        time: '',
        flop: {
            cards: [],
            sitbets: [
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
            ],
        },
        preflop: {
            cards: [],
            sitbets: [
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
            ],
        },
        turn: {
            cards: [],
            sitbets: [
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
            ],
        },
        river: {
            cards: [],
            sitbets: [
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
                { balance: 0, bet: 0 },
            ],
        },
        summary: [
            { cards: [], isFold: true, winloss: 0 },
            { cards: [], isFold: true, winloss: 0 },
            { cards: [], isFold: true, winloss: 0 },
            { cards: [], isFold: true, winloss: 0 },
            { cards: [], isFold: true, winloss: 0 },
        ],
        fullHistory: [],
    };
};
exports.makeHl = makeHl;
