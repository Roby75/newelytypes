export interface Card {
    suite: string;
    rank: string;
}
export interface Phase {
    cards: Card[];
    sitbets: SitBet[];
}
export interface SitBet {
    bet: number;
    balance: number;
}
export interface SitSummary {
    isFold: boolean;
    cards: Card[];
    winloss: number;
}
export interface HandLog {
    key: string;
    time: string;
    preflop: Phase;
    flop: Phase;
    turn: Phase;
    river: Phase;
    headers: {
        username: string;
    }[];
    summary: SitSummary[];
    fullHistory: string[];
}
export declare const makeHl: () => HandLog;
